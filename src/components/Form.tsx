import React from 'react'
import { Formik } from 'formik'
import Box from '@mui/material/Box';
import Input from '@mui/material/Input';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import dayjs from 'dayjs';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

import Button from './../components/Button'
import { DatePicker } from '@mui/x-date-pickers';

function Form() {
    const [vehicle, setVehicle] = React.useState('2021-tacoma');

    const handleChange = (event: SelectChangeEvent) => {
        setVehicle(event.target.value as string)
    }

    return (
        <Formik
        initialValues={{
                date: '',
                gallons: '',
                price: '',
                total: '',
                miles: 0
            }}
        validate={values => {
            const errors = {
                date: ''
            }
            if (!values.date) {
            errors.date = 'Required'
            }
            return errors
        }}
        onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
            alert(JSON.stringify(values, null, 2))
            setSubmitting(false)
            }, 400)
        }}
        >
        {({
            values,
            errors,
            touched,
            handleBlur,
            handleSubmit,
            isSubmitting
        }) => (
            <Box sx={{
                display: 'flex',
                flexWrap: 'wrap',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                marginY: 3,
                border: 1,
                borderColor: '#232',
                borderWidth: 3,
                backgroundColor: '#fff'
            }}>
                <div>
                    <FormControl variant='standard' sx={{ m: 1, mt: 3, width: '40ch' }}>
                        <Select
                            labelId='report-vehicle-label'
                            id='report-vehicle'
                            value={vehicle}
                            label='Vehicle'
                            onChange={handleChange}
                            aria-describedby='report-vehicle-text'
                            inputProps={{
                                'aria-label': 'vehicle',
                            }}>
                            <MenuItem value='2021-tacoma'>2021 Toyota Tacoma TRD Off-Road</MenuItem>
                            <MenuItem value='2020-corolla'>2020 Toyota Corolla LE Hybrid</MenuItem>
                            <MenuItem value='2011-corolla'>2011 Toyota Corolla S</MenuItem>
                            <MenuItem value='2009-tacoma'>2009 Toyota Tacoma</MenuItem>
                        </Select>
                        <FormHelperText id='report-vehicle-text'>Vehicle</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <DatePicker
                        label='Date'
                        sx={{ m: 1, width: '25ch' }}
                        defaultValue={dayjs()}
                        slotProps={{
                            textField: {
                                id: 'report-date'
                            }
                        }}/>
                </div>
                <div>
                    <FormControl variant='standard' sx={{ m: 1, mt: 3, width: '25ch' }}>
                        <Input
                            id='report-miles'
                            endAdornment={<InputAdornment position='end'>mi</InputAdornment>}
                            aria-describedby='report-miles-text'
                            inputProps={{
                                'aria-label': 'miles',
                            }}
                        />
                        <FormHelperText id='report-miles-text'>Miles</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant='standard' sx={{ m: 1, mt: 3, width: '25ch' }}>
                        <Input
                            id='report-gallons'
                            endAdornment={<InputAdornment position='end'>gal</InputAdornment>}
                            aria-describedby='report-gallons-text'
                            inputProps={{
                                'aria-label': 'gallons',
                            }}
                        />
                        <FormHelperText id='report-gallons-text'>Gallons</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl sx={{ m: 1, width: '25ch' }} variant='standard'>
                        <InputLabel htmlFor='report-price'>Price/Gallon</InputLabel>
                        <Input
                            id='report-price'
                            startAdornment={<InputAdornment position='start'>$</InputAdornment>}
                            endAdornment={<InputAdornment position='end'>/gal</InputAdornment>}
                        />
                    </FormControl>
                </div>
                <div>
                    <FormControl sx={{ m: 1, width: '25ch' }} variant='standard'>
                        <InputLabel htmlFor='report-cost'>Total Cost</InputLabel>
                        <Input
                            id='report-cost'
                            startAdornment={<InputAdornment position='start'>$</InputAdornment>}
                        />
                    </FormControl>
                </div>
                <div style={{ paddingTop: 15, paddingBottom: 30 }}>
                    <Button label='Record Fill-Up' icon='local-gas-station' theme='primary' />
                </div>
            </Box>
        )}
        </Formik>
    )
}

export default Form