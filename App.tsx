import { StatusBar } from 'expo-status-bar'
import { StyleSheet, View } from 'react-native'
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'

import Button from './src/components/Button'
import Report from './src/registry/Report'

export default function App() {
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <View style={styles.container}>
        <View>
          <Report />
        </View>
        <View>
          <Button label='Manage Vehicles' icon='directions-car' />
          <Button label='View Data' icon='data-usage' />
        </View>
        <StatusBar style='auto' />
      </View>
    </LocalizationProvider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#383f38',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
